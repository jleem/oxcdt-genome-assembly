# Genome assembly by alignment/mapping of nanopore reads to a reference

This document is an introduction for assembling Nanopore reads by-reference. Important aspects such as quality control of the initial dataset, and the usage of quality scores in the alignment, are not covered. This is covered by tools such as ```nanopolish``` and ```QUAST```.

### Software requirements:
- [poretools](https://github.com/arq5x/poretools) (Toolkit for working with Nanopore data)
- [LAST](http://last.cbrc.jp) (Alignment software for long reads)
- [samtools](http://www.htslib.org) (A suite of programs for interacting with HT sequencing data)
- [Tablet](https://ics.hutton.ac.uk/tablet/) (Alignment/Assembly Viewer)

To find out more about the options of a programme (poretools, samtools, lastdb etc.), type
```
[programme] --help
or
man [programme]
```

For a short descripton of file formats see [here](dataFormats.md).


#### Note
**In the following, "reads_folder" and "reference.fasta" need to be replaced by your respective read folder and reference file name**

## Genome Assembly
For plasmid alignments use 'allRef.fasta'. This file contains reference sequences for genes gacS, GFP, a cyanide synthesis gene, an alkaline protease, and a gentamycin resistance gene and the plasmid backbones pUC18 and pME6010. The alignment software will attempt to align your reads against all those sequences. At the end of this exercise you should know which of these genes and plasmids are present and whether there are any insertions, deletions or other mutations.

You are encouraged to add more reference sequences to the reference files as we go along.

Before we continue with the alignments, make sure you're in ```/tmp/```!
```
cd /tmp/nanoporeData
```

# Running on an example dataset 
As before with the *de novo* assembly tutorial, try this [toy dataset](https://figshare.com/s/edb77f39864497d272c2) first to get a feel for the tools.
This is a set of reads for the puc18 plasmid; as before, extract the data by

```
tar -xvf puc18_example.tar.gz
```

##### Step 1: Extract reads as FASTA
For our first example, we will run the tools on reads from the puc18 plasmid. 
Metrichor returns each of the basecalled reads as individual fast5 files. 
Use poretools to extract the reads from the fast5 folder and store them in a single fasta file with the following command:

```
poretools fasta reads_folder/ > reads.fasta
```

Note that we could also have used the fastq format which includes the quality scores and may help with the alignment step. However, for simplicity we will use the fasta format here.

##### Step 2: Generate index files
Create the index files required for sequence comparison and alignment by running the following command:

Alignment tools like to index their files to improve computational efficiency. Provided you are working in a directory such as /tmp/nanoporeData/puc18, and the reference file is named 'puc18.fasta', the command would look like this:

Note that "reference" in reference.lastindex should be replaced by your reference file name.

```
lastdb -Q 0 reference.lastindex reference.fasta
```
```
*HELP*
-Q	input format: 0=fasta, 1=fastq
```

This will generate a set of files with different extensions (.ssp, .tis, .sds, .des, .prj, .suf, .bck). These are used by the alignment program, so make sure to keep these!

##### Step 3: Alignment
Align the extracted 2d reads to the reference sequence with the following command (Nick Loman suggested the parameters, which work quite well):

```
lastal -s 2 -T 0 -Q 0 -a 1 reference.lastindex reads.fasta > reads_aligned.maf
```
```
*HELP*
-s	0=reverse, 1=forward, 2=both
-T	type of alignment: 0=local, 1=overlap
-Q	input format: 0=fasta, 1=fastq
-a	gap penalty
```

If you would like to know more about how **LAST** works, see this [slide deck](http://last.cbrc.jp/mcf-kyoto08.pdf).

##### Step 4: Generate Genome Viewer friendly alignment
Convert your alignment to the .sam format with maf-convert.py (part of the LAST package):

```
maf-convert sam reads_aligned.maf > reads_aligned.sam
```

Index your reference file:

```
samtools faidx reference.fasta
```

Compress .sam to .bam:

```
samtools view -b -t reference.fasta.fai -o reads_aligned.bam reads_aligned.sam
```
```
*HELP*
-b	output BAM
-S	input SAM
-t	reference index file
-o	output file name
```

Sort your alignment by genome location to allow for pile-up:

```
samtools sort -T reads reads_aligned.bam -o reads_aligned.sorted.bam
```

Index the sorted to alignment (required to view the alignment in tablet)
```
samtools index reads_aligned.sorted.bam
```

#### Step 5: Alignment Visualisation

- Open the Tablet alignment viewer. In the terminal type:
```
tablet
```

- Go to >Open Assembly.
- Select your sorted alignment file (```reads_aligned.sorted.bam```).
- Select your reference (```puc18.fasta```).
- In the left column under contigs select your alignment.
- You should now be able to see your assembly.
- To see the coverage, go to > Advanced > Coverage.

![Tablet alignment](figures/tablet.png)

*Figure 1.* An alignment of Nanopore reads of E.coli as viewed in Tablet.

###### Script
#### This script includes all commands shown in the section above.
```
#/bin/bash

barcode="2D"
reference="puc18"
reference_folder="puc18Example"

lastdb -Q 0 $reference_folder/$reference".lastindex" $reference_folder/$reference".fasta"
echo "LAST Database created!"

lastal -s 2 -T 0 -Q 0 -a 1 $reference_folder/$reference".lastindex" $barcode"_reads.fasta" > $barcode"_reads_aligned.maf"
echo "LAST alignment done..."

maf-convert sam $barcode"_reads_aligned.maf" > $barcode"_reads_aligned.sam"
echo "Converting MAF to SAM file..."

samtools faidx $reference_folder/$reference".fasta"
echo "SAMtools index prepared..."

samtools view -b -t $reference_folder/$reference".fasta.fai" -o $barcode"_reads_aligned.bam" $barcode"_reads_aligned.sam"

echo "Sorting by SAMtools..."
samtools sort -T $barcode $barcode"_reads_aligned.bam" -o $barcode"_reads_aligned.sorted.bam"

samtools index $barcode"_reads_aligned.sorted.bam"
```
The following commands can be run with [this script](alignmentScript.sh). Replace the values for "barcode" and "reference" accordingly.


Place the script under /tmp/nanoporeData/ and run it:

```
bash alignmentScript.sh
```

You may have to give it execution permissions first, like so:

```
chmod +x alignmentScript.sh
```

# Running on your own dataset
##### Step 1: Extract reads
Now that you know how to run LAST from start to finish, extract your reads into FASTA files using poretools. The reference sequences are [here](https://figshare.com/s/bf8a8eb4df8a3be21458).

##### Step 2: Run the alignment script
With your script, change some of the parameters at the start (e.g. the "barcode", the "reference_folder") to match where your reads and reference are. 
Now that you are using your own data, choose your reference wisely -- what types of things should you consider when choosing a reference?

##### Step 3: Characterise Samples
How many reads are aligning against your reference sequences? 
What does this tell you about your sample? Can you say which of the following samples have been successfully sequenced?

##### Plasmids
* pME3258 GacS
* pME6191 GacS Δ76
* pME3283 GacS H294R
* pME3285 GacS H863R
* pME3284 GacS D717N
* pME3281 GacS

[This paper](http://apsjournals.apsnet.org/doi/abs/10.1094/MPMI.2003.16.7.634) will help you identify the different gac variants:

* Zuber, Sophie, et al. "GacS sensor domains pertinent to the regulation of exoproduct formation and to the biocontrol potential of Pseudomonas fluorescens CHA0." Molecular plant-microbe interactions 16.7 (2003): 634-644.

Do as much as you can given the coverage you have. In the following step we provide a larger dataset, which should make your life easier.

# Interpret bacterial phenotypes
Use all the information you have gathered to date to answer the questions on p21 of the handbook. Write up your findings in your lab book, explaining your reasoning.

# Feel free to collaborate.

### Further reading
- [How LAST works](http://last.cbrc.jp/mcf-kyoto08.pdf)
- Tutorial on [samtools](http://biobits.org/samtools_primer.html)
- Beginner’s guide to comparative bacterial genome analysis using next-generation sequence data [DOI: 10.1186/2042-5783-3-2](http://microbialinformaticsj.biomedcentral.com/articles/10.1186/2042-5783-3-2)
- Treangen et al. "[Repetitive DNA and next-generation sequencing: computational challenges and solutions.](http://www.nature.com/nrg/journal/v13/n1/full/nrg3117.html)" Nature Reviews Genetics 13.1 (2012): 36-46.
- [Slides on genome analysis](http://schatzlab.cshl.edu/teaching/) by the Schatz lab
