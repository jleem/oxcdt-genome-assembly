# *De novo* assembly with nanopore reads

This document introduces a typical workflow for the *de novo* assembly of Nanopore reads. Due to the unique characteristics of Nanopore reads and continued improvements in chemistry and software, new strategies for assembly are being developed continuously. 

Here, we will briefly describe one particular workflow that is based on a package called ```Canu```, a *de novo* assembler specific for long noisy reads, and this is a *very* quick guide to how Canu works.

```
Several steps are required for the de novo assembly of raw sequencing data. Thus, a de novo assembler
may be regarded as a collection of modules that takes in a large number of raw sequencing reads
and puts out a consensus sequence.
```

### *De novo* assembly with Canu
Canu operates in three main stages: Correction, Trimming and Unitigging.

##### Stage 1: Correction
Due to the relatively high noise of Nanopore data, error correction is essential to ensure successful *de novo* assembly. This step has previously been done with hybrid approaches that used accurate short reads to correct the low accuracy long reads. However, it has been shown that a set of long reads with a certain coverage may be sufficient for self-correction. Canu for example identifies a set of target reads and uses overlapping reads from the entire dataset for correction. Canu uses a probabilistic algorithm called MinHash, which converts sequences into a low-dimensional representation (known as hashing), to find overlaps. The overlaps are then used to find a consensus via the FALCON-sense algorithm.

![hashing diagram](figures/hashing.png)

*Figure 1.* Hashing refers to a function where a sequence of letters, such as names, are converted to a pre-defined data format (say, integers). If you would like to have a go at understanding how MinHash does it, check out the [figure in their paper](https://www.nature.com/articles/nbt.3238/figures/1).

##### Stage 2: Trimming
Once the reads have been corrected, the reads are cut into shape to remove any low quality sequence from the ends. The trimming stage selects the biggest segment for each corrected read so that the error, overlap and coverage are above selected thresholds.

##### Stage 3: Unitigging
The corrected and trimmed reads are now ready to be stitched back together into their original configuration. A unitigger algorithm finds the best overlap on each end of each read and generates so-called unitigs. A unitig contains a high confidence, contiguous, unique sequence up to a boundary that is defined by a branch (i.e. a point of ambiguity) in the overlap graph. Canu deploys the bogart algorithm (best overlap graph) for this purpose.

![overlap graph diagram](figures/Overlap.png)

*Figure 2.* An overlap graph can be built based on the overlap of the reads. Suppose you have sequences as shown on top The reads that have overlaps with another read are connected by arrows. Using this overlap information, you can reconstruct the ordering of the reads. Figure from [Henrik Lantz](http://slideplayer.com/slide/5255480/)

### Running canu
The default command for running canu with nanopore reads is:

```
canu -p [PREFIX] -d [DIRECTORY_NAME] genomeSize=[ESTIMATED_GENOME_SIZE] -nanopore-raw [FASTQ/FASTA FILES]
```

This generates a set of files for each of the three stages (including html reports). Your consensus file will be stored in the current folder. Canu can also be run independently for the different stages.

##### Step 0: Set up canu for your machine.
For this exercise, you should setup your machine to run canu. Set up your PATH variable (make sure to type this exactly, and do this for every new Terminal window/tab):
```
export PATH="/usr/local/canu-1.3/Linux-amd64/bin/:$PATH"
```
If you now type ```which canu``` you should get:

```
/usr/local/canu-1.3/Linux-amd64/bin/canu
```
Since nanopore assemblies generate lots of files, go to ```/tmp/``` (```cd /tmp/```).

# Running canu on an example dataset

##### Step 1: Use established E.coli data
Download model reads from the Loman group; type

```
wget http://nanopore.s3.climb.ac.uk/MAP006-1_2D_pass.fasta
```

You should get a new ```MAP006-1_2D_pass.fasta``` file in your current folder (```/tmp/```). 
This is a set of reads from Nick Loman on an E.coli genome. Run the assembly with canu using the following parameters:

```
canu -p ekoli -d ecoli_folder -errorRate=0.025 -useGrid=false -maxThreads=2 -genomeSize=4.6m -nanopore-raw MAP006-1_2D_pass.fasta
```

This command is basically saying...
* Create a new folder named ```ecoli_folder```, where
* All my files have the prefix ```ekoli``` for their filenames;
* Set an overlap error rate of 2.5% (check the [canu paper](http://genome.cshlp.org/content/27/5/722.full) for more details)
* do NOT use grid computing for this task.
* use up to 2 threads for parallelisation.
* Estimate that the genome is around 4.6million base pairs.
* The file MAP006-1_2D_pass.fasta is a raw set of reads from Nanopore; hence `-nanopore-raw`.

Canu does take **a while** (~2.5 hours), so leave this running in the background. 

In the meantime, download MUMmer (which we will use later for validating the alignments). Visit [here](https://sourceforge.net/projects/mummer/files/mummer/3.23/) and download the MUMmer3.23.tar.gz file. Then run

```
tar -xzvf MUMmer3.23.tar.gz
cd MUMmer3.23
make check
make install
```

After some crazy printing on your screen, you should have a new tool that you can open in this directory called ```dnadiff```. 
Only go to step 2 once your canu assembly is done. I would suggest doing the [alignment by-reference section](alignment.md) while you wait.

##### Step 2: validate assembly with reference genome.
If you are reading this section I am assuming canu was run on the reads; you should now have a fully assembled genome in your folder ```ecoli_folder```. Helpfully the assembly should be named ```ekoli.contigs.fasta```.

Download the K-12 reference genome from the NCBI, then unzip it.
```
wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/005/845/GCF_000005845.2_ASM584v2/GCF_000005845.2_ASM584v2_genomic.fna.gz
tar -xvf GCF_000005845.2_ASM584v2_genomic.fna.gz
```

Compare your assembly with the reference sequence using ```dnadiff```
```
cd /wherever/mummer/is/MUMmer3.23/
./dnadiff /wherever/reference/is/GCF_000005845.2_ASM584v2_genomic.fna /tmp/ecoli_folder/ekoli.contigs.fasta
```

You should get a new report file under ```/wherever/mummer/is/MUMmer3.23/``` called ```out.report```. What is the average sequence identity for the M-to-M alignment?

# Running on your data
Now that you have had the chance to run canu on a nice toy example, run canu on your data!

##### Step 1: Extract 2D reads as FASTA
Metrichor returns each of the basecalled reads as individual fast5 files. 
Use poretools to extract the reads from your barcoded data and store them in a single fasta file with the following command:

```
poretools fasta reads_folder/ > 2Dreads.fasta
```

##### Step 2: Use the FASTQ reads to build your genome *de novo*
Using the reads, then run canu, as before:

```
canu -p [PREFIX] -d [DIRECTORY_NAME] genomeSize=[ESTIMATED_GENOME_SIZE] -nanopore-raw [FASTQ/FASTA FILES] -useGrid=false -maxThreads=2 &> canu_output &
```

Once the process is done, you will find your assembled contigs in the folder that you used for the ```-d``` flag. Remember that you can specify an error threshold (as before). A lower error rate means that a more stringent overlap cutoff is used, while a higher error rate will allow more relaxed overlaps in contig assembly.

Following the assembly, it is possible that you will have multiple contigs. Do not fear! This is all part of the process, and you may not always have one neat genome assembly out of canu. Then ask yourself this: why do we get multiple contigs, and not one neat solution?

##### Step 3: Assess the quality of the assembly

You can use ```dnadiff``` to check the similarity between your assembled genome and a reference sequence by

```
./dnadiff [REFERENCE_SEQUENCE] [ASSEMBLED_CONTIG.FASTA]
```

- For more details on the Canu algorithm, see [here](http://genome.cshlp.org/content/27/5/722.full)
- How does MinHash work? See [here](https://www.nature.com/articles/nbt.3238)
- What is FALCON-Sense? See [here](https://images.nature.com/original/nature-assets/nmeth/journal/v13/n12/extref/nmeth.4035-S1.pdf)
- For assessing your assembly see e.g. [QUAST](http://bioinf.spbau.ru/quast)
- For annotating your assembly see e.g. [Patric](https://www.patricbrc.org/portal/portal/patric/Home)
