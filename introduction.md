# Introduction to next-generation sequencing methods
Advancements in DNA sequencing technologies have allowed for the rapid and relatively cost effective characterization of whole genomes. This has opened the door for a variety of applications
in areas such as evolutionary biology, medical research, metagenomics and molecular biology. However, due to fragmentation of the genome during the preparation of the DNA sequencing library,
the **order** of the individual fragments is lost:

![genome assembly figure](figures/nmeth.1935-I2.jpg)

*Figure 1* Basic overview of genome assembly. Figure from [Baker](https://www.nature.com/articles/nmeth.1935).

Thus, the sequenced fragments (reads) need to be correctly stitched back together into their *original configuration* (i.e. ordered), a process called **genome assembly**.

**Consider this**: a researcher observes a known bacterial strain with an unusual phenotype. He/she would like to sequence the genome to identify the responsible genetic change. Genome assembly using a reference sequence (i.e. mapping) would allow for the identification of small alterations in the sequence such as single nucleotide polymorphisms (SNPs), insertions or deletions. 

However, larger alterations such as duplication events that are **not** in our reference sequence would be lost. A better approach for detecting these kinds of new structural variations is *de novo* assembly. This method requires no prior knowledge of the original sequence, but instead attempts to reconstruct the genome from the reads *only*.

(**TL;DR**) Reasons for...

|Assembly by alignment (mapping)|*De novo* assembly|
|---||---|
|Small variations are easily detectable||Does not require a reference|
|More likely to generate a single consensus||Useful for detecting structural variations| 

```
Structural variations include inversions, translocations as well as copy number variations (CNVs),
i.e. >1kb deletions and insertions.
```

Reasons not to...

|Assembly by alignment (mapping)|*De novo* assembly|
|---||---|
|New information can be swamped||Computationally expensive|
|Sequence is only as good as the reference||Can lead to fragmented assemblies with poor coverage|

In this practical we will treat mapping and *de novo* assembly as independent methods, but the two methods can be used to complement each other.

## Assembly by mapping/alignment/by-reference
Mapping, also referred to as the alignment of reads against a reference sequence, is commonly used in re-sequencing.  As the name suggests, the reads are mapped one by one to an existing reference sequence. As the number of aligned sequences grows, reads will start to overlap and form contiguous stretches of genomic sequence. Thus, by aligning more and more reads to the reference a stacked set of overlapping sequences is built up, commonly referred to as a pile-up. Depending on the **coverage** and **read accuracy**, we can then calculate the consensus sequence with a certain confidence. Given that the confidence is high enough, we may compare the consensus against the reference to detect genetic variations, such as insertions, deletions and SNPs.

```
The coverage tells us how many times on average a single base has been covered by a read; defined as LN/G,
where L is the read length, N is the number of reads and G is the size of the reference sequence.
```

![pile-up](figures/pile-up.png)

**Figure 2** A schematic showing a pile-up generated during alignment. The reads and reference are shown in blue and green, respectively. The total length of the reads is 5.6x that of the reference, i.e. the coverage of our alignment is 5.6x.

##### Challenges of mapping/alignment:
- Reads do not include position information, thus we need to find the correct region in the reference sequence.
- Choosing a suitable cutoff for mismatch penalties. Make it too high and we would never see any variation. Make it too low and our alignments would be wrong.
- Filter out sequencing errors. Allow for a low level of sequencing errors in our reads, and filter them out later.

## *De novo* Assembly
*De novo* assembly aims to reconstruct the original genome sequence from the reads *alone*. Generally, the first step is to find overlaps between the reads and build a graph to describe their relationship. The graph is then simplified and a consensus is extracted. Several approaches have been used for this purpose (e.g. de Bruijn graphs).  A core step in this process is the assembly of contiguous, unambiguous stretches of DNA sequence, so called contigs. Contigs essentially are local consensus sequences that result from the assembly of overlapping reads.

One of the most challenging steps during assembly used to be repeats; DNA regions that have multiple copies in the genome, such as transposons, satellites and gene duplications. These regions are potentially problematic, and they can be hard to place in the correct genomic location if the read lengths are **shorter than the repeat**. However, with long reads generated by 3rd generation sequencers such as Oxford Nanopore’s MinION, this problem has become largely obsolete. 

```
Long reads enable easier mapping of repetitive sequences, as the reads now span across these regions.
This allows for their correct placement in the assembly.
```

![longvsshort](figures/longvsshort.png)

**Figure 3** Schematic highlighting the advantages of long reads in *de novo* assembly of A) tandem repeats and B) copy number variations. 

Read accuracy may also be a potentially limiting factor to successful *de novo* assembly.  However, advancements in assemblers (e.g. hierarchical genome-assembly process (HGAP)) have allowed for the self-correction of reads, which makes highly contiguous assemblies possible. See the [study by Loman et al.](http://www.nature.com/nmeth/journal/v12/n8/full/nmeth.3444.html) where a complete bacterial genome was assembled de novo with 99.5% nucleotide identity.

### Dataset
As we mentioned, use another group's reads if your dataset isn't available. There are other model datasets out there (which you'll see later in the tutorial!)

### Data formats
For a short description of the data formats, see [here](dataFormats.md).

### Further reading:

##### Theory
- A general [review paper](https://www.nature.com/articles/nrg.2016.49) on next-generation sequencing approaches.
- [Beginner’s guide to comparative bacterial genome analysis using next-generation sequence data.](http://microbialinformaticsj.biomedcentral.com/articles/10.1186/2042-5783-3-2)
- [Do-it-yourself guide to genome assembly](https://academic.oup.com/bfg/article/15/1/1/1741842)
- [How to apply de Brujin graphs to genome assembly](https://www.nature.com/articles/nbt.2023)
- [De novo genome assembly: what every biologist should know](https://www.nature.com/articles/nmeth.1935)
- Some presentations on genome analysis by the [Schatz lab](http://schatzlab.cshl.edu/teaching/).

##### Other tutorials
- Great tutorial on next-gen sequencing genome analysis using [samtools](http://biobits.org/samtools_primer.html) (also covers SNP detection).
- [Celera (Canu) Assembler Terminology](http://wgs-assembler.sourceforge.net/wiki/index.php/Celera_Assembler_Terminology) (some of it is irrelevant to nanopore reads e.g. mate-pairs).
- [How to score genome assemblies](https://code.google.com/p/ngopt/wiki/How_To_Score_Genome_Assemblies_with_Mauve) with [Mauve](http://darlinglab.org/mauve/mauve.html)
- [QUAST: quality assessment tool for genome assemblies](http://bioinformatics.oxfordjournals.org/content/29/8/1072.abstract)
- [Using canu for PacBio data](https://bpa-csiro-workshops.github.io/btp-manuals-md/modules/btp-module-denovo-canu/denovo_canu/)
