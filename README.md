# Genome Assembly tutorial

## The Exercise
[In this exercise](https://bitbucket.org/jleem/oxcdt-genome-assembly/) you will learn about nanopore read characterisation, genome alignment and alignment analysis.

### Sequencing Data
For today's practical, you were in one of 7 groups (as a pair), and you sequenced one of seven plasmids. After you gave us your barcoded libraries on Friday, we quantified them. Using those concentrations, we pooled them and sequenced them on an Oxford Nanopore flow cell.

Unfortunately, not all samples were sucessfully sequenced. This could be due to a variety of reasons.
For groups with no reads or a low number of reads, look at the samples of other pairs. 

The following table shows where the sequencing data can be found:

|Barcode|URL|
|-------|----|
|barcode01|[Link](https://figshare.com/s/cf55f636b2ea524060f3)|
|barcode02|[Link](https://figshare.com/s/f4ba7a26ff79fb7099be)|
|barcode03|[Link](https://figshare.com/s/112026f3a4e57d0b7d3b)|
|barcode04|[Link](https://figshare.com/s/05d76f5d18747573160e)|
|barcode05|[Link](https://figshare.com/s/8b6bff76c4b18792dfb7)|
|barcode06|[Link](https://figshare.com/s/2a73c2d74819d1fdd62a)|
|barcode07|[Link](https://figshare.com/s/f33b8c2cc93d15eca589)|

Today, you will be tasked with the following:

- [Use poretools](poretools.md) to characterise the sequencing data
- [*De novo* assembly](deNovoAssembly.md) using your sequence data to build *de novo*
- [Assemble by reference](alignment.md) using your sequence data and some given references

For the *de novo* assembly and reference by-assembly, you will need the following:

|Section|Dataset|Link|
|-------|-------|----|
|*De novo* assembly|E. coli genome|[Nick Loman's MAP006-1 fasta file](http://nanopore.s3.climb.ac.uk/MAP006-1_2D_pass.fasta)|
|Assembly by reference|reference sequences|Check [here](https://figshare.com/s/bf8a8eb4df8a3be21458)|
|                     |puc18 toy dataset|Check [here](https://figshare.com/s/edb77f39864497d272c2)|

    
## Objectives:
The tutorial aims to provide

- a basic understanding of genome assembly
- a workflow for *de novo* assembly
- a workflow for assembly by alignment 

## Tutorial structure
1. [Introduction to genome assembly](introduction.md)
2. [Using poretools to analyse data](poretools.md)
3. [*De-novo* assembly - workflow example](deNovoAssembly.md)
4. [Assembly by alignment - workflow example](alignment.md)

This tutorial is largely due to the ground work done by [Sam Demharter](https://github.com/demharters). Since *de novo* assembly takes quite a while, I suggest letting that run on the example sequence first in the background, then do the assembly by-alignment, then revisit the *de novo* assembly page.

### Something we'd like to squeeze in here...

The tutorial assumes that you'll run this in Linux or Mac. Some useful commands for either OS include...

|Command|Action|Example|
|---|---|---|
|mkdir|make directory|mkdir folderName|
|cd|change directory|cd folderName|
|  |go up one directory|cd ..|
|pwd|print working directory|pwd|
|rm|remove file|rm fileName|
|  |remove folder|rm -r folderName|
|mv|move or rename file or folder|mv oldFolderName newFolderName|
|cp|copy file|cp file fileCopy|
|cp -r|copy folder|cp -r folder folderCopy|
|ls|list folder contents|ls folderName|
|  |list folder contents; show most recent file at bottom, size (-h) and ownership (-l) |ls -ltrh|

For a more comprehensive set of commands, check [here](http://linuxcommand.org/lc3_learning_the_shell.php).

* Popular tools for opening text files in Linux are **vim** (Terminal) and **gedit** (GUI).
* Tools for opening pdfs and images in Linux include **evince**, and **okular**.
