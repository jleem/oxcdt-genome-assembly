#/bin/bash

##### CHANGE THE BARCODE, REFERENCE AND REFERENCE_FOLDER AS NECESSARY #####
##### This script is intended to be a guide, make sure you know what's happening at each step #####

barcode="2D"
reference="puc18"
reference_folder="puc18Example"

lastdb -Q 0 $reference_folder/$reference".lastindex" $reference_folder/$reference".fasta"
echo "LAST Database created!"

lastal -s 2 -T 0 -Q 0 -a 1 $reference_folder/$reference".lastindex" $barcode"_reads.fasta" > $barcode"_reads_aligned.maf"
echo "LAST alignment done..."

maf-convert sam $barcode"_reads_aligned.maf" > $barcode"_reads_aligned.sam"
echo "Converting MAF to SAM file..."

samtools faidx $reference_folder/$reference".fasta"
echo "SAMtools index prepared..."

samtools view -b -t $reference_folder/$reference".fasta.fai" -o $barcode"_reads_aligned.bam" $barcode"_reads_aligned.sam"

echo "Sorting by SAMtools..."
samtools sort -T $barcode $barcode"_reads_aligned.bam" -o $barcode"_reads_aligned.sorted.bam"

samtools index $barcode"_reads_aligned.sorted.bam"
