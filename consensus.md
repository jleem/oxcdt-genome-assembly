Generate a consensus sequence

Download bcftools and htslib from [here](http://www.htslib.org/download/)

Extract the archive.
```
tar -xvf bcftools-1.6.tar.bz2
tar -xvf htslib-1.6.tar.bz2
```

Go to the folder
```
cd bcftools-1.6
```

Configure your installation,
```
./configure --prefix='/auto/gemini/[USER]'
```

where ```[USER]``` is your username. Then type

```
make install
```
and you now have bcftools ready to go. Do the same for htslib.

To generate your consensus sequence, do
```
samtools mpileup -g -f [REFERENCE_SEQUENCE] [SORTED_BAM_FILE] > variant.bcf
/path/where/bcftools/is/bcftools call -c -v variant.bcf > variant.vcf
/path/where/bcftools/is/bcftools filter -i '%QUAL>=[SCORE]' variant.vcf > variant_filtered.vcf

/path/where/htslib/is/bgzip variant_filtered.vcf
/path/where/htslib/is/tabix variant_filtered.vcf.gz

cat [REFERENCE_SEQUENCE] | /path/where/bcftools/is/bcftools consensus variant_filtered.vcf.gz > consensus.fa

```
