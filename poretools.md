# Using poretools for initial analysis

## Summary
In this exercise we introduce a simple pipeline for the 

### Software
- [poretools](https://github.com/arq5x/poretools) (Toolkit for working with Nanopore data)

## The Exercise
Due to data storage requirements, work in ```/tmp/```! First download your data, then make a new folder for your barcode, e.g.

```
cd /tmp/
mkdir barcode01
```

Once you download your barcode0X.tar.gz (where X is your barcode number..) then run

```
tar -xvf barcode0X.tar.gz
```

This should give you a new folder named ```barcode0X```, and in it, there will be one or more folders with the prefix ```batch```.


### Explore your data
```poretools``` can help you characterise your nanopore reads.
**In the following commands, "reads_folder" needs to be replaced with your respective read folder path, for example with "/datasets/run1/BC03".**

Plot the total yield of your run:
```
poretools yield_plot --plot-type reads --saveas reads_yield.pdf reads_folder
```
*To plot the total basepair count set --plot-type to "basepairs"*

Change into the ```reads_folder``` directory, and view the ```reads_yield.pdf``` file, e.g.

```
evince reads_yield.pdf
```

Plot the distribution of read lengths:
```
poretools hist reads_folder --saveas readLength_hist1.pdf
```

You can modify your plot by adding the following options:
```
poretools hist --min-length 1000 --max-length 10000 reads_folder --saveas readLength_hist2.pdf
poretools hist --num-bins 20 --max-length 10000 read_folder --saveas readLength_hist3.pdf
```

Look at the throughput of each of the pores:
```
poretools occupancy reads_folder --saveas occupancy.pdf
```

For more commands see the [poretools documentation](https://poretools.readthedocs.io/en/latest/content/examples.html).
